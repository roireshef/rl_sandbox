from abc import ABCMeta

from rl_framework.src.common.encodable import Encodable


class State(Encodable, metaclass=ABCMeta):
    def __init__(self, is_terminal: bool):
        self.is_terminal = is_terminal

    def __str__(self):
        return '%s(%s)' % (self.__class__.__name__, self.__dict__)
