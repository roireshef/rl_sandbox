from abc import ABCMeta

from rl_framework.src.common.encodable import Encodable


class Action(Encodable, metaclass=ABCMeta):
    def __init__(self, id: int):
        self.id = id

    def __str__(self):
        return '%s(%s)' % (self.__class__.__name__, self.__dict__)