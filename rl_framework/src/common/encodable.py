from abc import abstractmethod, ABC


class Encodable(ABC):
    @abstractmethod
    def numpy(self):
        pass

    @abstractmethod
    def torch(self):
        pass
