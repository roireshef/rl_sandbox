from abc import abstractmethod, ABCMeta
from typing import List

from rl_framework.src.common.action import Action
from rl_framework.src.common.state import State


class Environment(metaclass=ABCMeta):
    def __init__(self):
        self._state_id = 0
        self._episode_id = 0

    @property
    @abstractmethod
    def state(self):
        pass

    @abstractmethod
    def get_valid_actions(self, state: State) -> (List[Action], List[bool]):
        pass

    @abstractmethod
    def get_all_actions(self) -> List[Action]:
        pass

    @abstractmethod
    def _step(self, action: Action) -> (State, float):      # override this with custom logic
        pass

    @abstractmethod
    def _reset(self) -> State:                      # override this with custom logic
        pass

    def step(self, action: Action) -> (State, float):       # do not override this
        self._state_id += 1
        return self._step(action)

    def reset(self) -> State:                       # do not override this
        self._state_id = 0
        self._episode_id += 1
        return self._reset()
