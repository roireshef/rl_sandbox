from typing import NamedTuple, List

from rl_framework.src.common.action import Action
from rl_framework.src.common.state import State
import numpy as np


class Transition(NamedTuple):
    state: State
    action: Action
    reward: float
    next_state: State


class TransitionsBuffers(NamedTuple):
    states: np.ndarray
    actions: np.ndarray
    rewards: np.ndarray
    next_states: np.ndarray
    is_terminals: np.ndarray
