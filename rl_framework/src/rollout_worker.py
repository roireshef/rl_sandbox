from abc import abstractmethod, ABCMeta
from time import sleep
from typing import Optional

import torch
from torch import multiprocessing as mp

from rl_framework.src.agent.agent import Agent
from rl_framework.src.common.environment import Environment
from rl_framework.src.gym_environment import GymEnvironment
from rl_framework.src.infra.buffer import SimpleBuffer, TransitionsSimpleBuffer
from rl_framework.src.infra.connection import Connection
import numpy as np


class RolloutWorker(metaclass=ABCMeta):
    def __init__(self, id: int, agent: Agent, transitions_connection: Connection, msg_size: Optional[int],
                 episode_counter: mp.Value):
        """

        :param id:
        :param agent:
        :param transitions_connection:
        :param msg_size: if None worker will send all transitions when episode is over, otherwise every
        <msg_size> transitions
        """
        self._id = id
        self._agent = agent
        self._transitions_connection = transitions_connection
        self._local_buffer = TransitionsSimpleBuffer() #SimpleBuffer()
        self._episode_reward_buffer = SimpleBuffer()
        self._cumulative_reward = 0
        self._msg_size = msg_size
        self._episode_counter = episode_counter

    @property
    @abstractmethod
    def env(self) -> Environment:
        pass

    def run(self):
        with torch.no_grad():
            if self.env.state.is_terminal:
                self.env.reset()
                self._episode_reward_buffer.put(self._cumulative_reward)
                self._cumulative_reward = 0

            state = self.env.state
            actions, mask = self.env.get_valid_actions(state)
            action = self._agent.choose_action_stochastic(state, actions, mask)
            new_state, reward = self.env.step(action)

            self._cumulative_reward += reward

            self._local_buffer.put(state.numpy(), action.numpy(), [reward], new_state.numpy(), [new_state.is_terminal])

            if (self._msg_size is not None and len(self._local_buffer) == self._msg_size) or \
                    (self._msg_size is None and new_state.is_terminal):
                sleep(0.5)
                # send transitions, episode_rewards
                self._transitions_connection.send(([self._local_buffer.get_all()], self._episode_reward_buffer.get_all()))


class LocalGymRolloutWorker(RolloutWorker):
    def __init__(self, id: int, agent: Agent, transitions_connection: Connection, msg_size: Optional[int],
                 episode_counter: mp.Value, environment_name: str):
        super().__init__(id, agent, transitions_connection, msg_size, episode_counter)
        self._gym_env = GymEnvironment(environment_name)

    @property
    def env(self) -> Environment:
        return self._gym_env


class RemoteGymRolloutWorker(RolloutWorker, mp.Process):
    def __init__(self, id: int, agent: Agent, transitions_connection: Connection, msg_size: Optional[int],
                 episode_counter: mp.Value, environment_name: str):
        mp.Process.__init__(self)
        RolloutWorker.__init__(self, id, agent, transitions_connection, msg_size, episode_counter)
        self._gym_env = GymEnvironment(environment_name)
        np.random.seed(id)

    def run(self):
        while True:
            super().run()

    @property
    def env(self):
        return self._gym_env
