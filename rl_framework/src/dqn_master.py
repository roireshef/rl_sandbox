import itertools
from ctypes import c_int
from typing import List

import numpy as np
import torch
import torch.multiprocessing as mp
from torch.nn.modules.loss import _Loss
from torch.optim.lr_scheduler import ExponentialLR

from rl_framework.src.agent.agent import Agent
from rl_framework.src.agent.exploration_policy import EpsilonGreedy, MaxEpsilonGreedy
from rl_framework.src.common.action import Action
from rl_framework.src.common.state import State
from rl_framework.src.dqn_net import DQNNet
from rl_framework.src.infra.buffer import TransitionsRingBuffer
from rl_framework.src.infra.mp_queue_connection import MultiProcessQueueConnection
from rl_framework.src.plot import plot_durations
from rl_framework.src.rollout_worker import RemoteGymRolloutWorker


class DQNAgent(Agent):
    def __init__(self, net: DQNNet, target_net: DQNNet, exploration_policy: EpsilonGreedy,
                 loss_func: _Loss, gamma: float):

        super().__init__()
        self.net = net
        self.target_net = target_net
        self.exploration = exploration_policy
        self.gamma = gamma
        self._loss = loss_func

    @torch.no_grad()
    def choose_action_stochastic(self, state: State, actions: List[Action], action_mask: List[bool]):
        action_values = self.net.forward(state.torch()).numpy()
        return self.exploration.choose_action(actions, action_mask, action_values, only_exploit=False)

    @torch.no_grad()
    def choose_action_deterministic(self, state: State, actions: List[Action], action_mask: List[bool]):
        action_values = self.net.forward(state.torch()).numpy()
        return self.exploration.choose_action(actions, action_mask, action_values, only_exploit=True)

    def update_num_episodes(self, num_episodes: int):
        self.exploration.update(num_episodes)

    def loss(self, s, a, r, s_tag, is_terminal):
        with torch.no_grad():
            target = r
            target[~is_terminal] += self.gamma * torch.max(self.target_net.forward(s_tag[~is_terminal.squeeze()]), dim=-1)[0]

        return self._loss(self.net.forward(s).gather(1, a), target).sum()

    def update_target(self):
        self.target_net.load_state_dict(self.net.state_dict())


if __name__ == "__main__":
    from gym.envs import register
    register(
        id='CartPole-v3',
        entry_point='gym.envs.classic_control:CartPoleEnv',
        max_episode_steps=10000,
        reward_threshold=9900.0,
    )

    REPLAY_BUFFER_SIZE = 1000000
    MINIBATCH_SIZE = 256
    LR_ALPHA = 0.005
    GAMMA = 0.99

    net = DQNNet(4, 200, 2).share_memory()
    target_net = DQNNet(4, 200, 2).share_memory()

    ep = mp.Value(c_int, 0)
    exploration = MaxEpsilonGreedy(init_eps=1.0, min_eps=0.1, decay=0.9975, shared_num_ep=ep)
    agent = DQNAgent(net=net, target_net=target_net, exploration_policy=exploration,
                     gamma=GAMMA, loss_func=torch.nn.MSELoss())

    agent.update_target()

    global_optimizer = torch.optim.SGD(agent.net.parameters(), lr=LR_ALPHA)

    transitions_queue = MultiProcessQueueConnection()

    rollout_workers = [RemoteGymRolloutWorker(id=i, agent=agent, transitions_connection=transitions_queue,
                                              msg_size=None, episode_counter=ep, environment_name='CartPole-v3')
                       for i in range(mp.cpu_count())]

    for rw in rollout_workers:
        rw.daemon = True
        rw.start()

    rbuffer = TransitionsRingBuffer(REPLAY_BUFFER_SIZE, np.empty(4, dtype=np.float32))
    tlist = []

    while len(rbuffer) < 2048:
        transitions = []
        if not transitions_queue.is_empty():
            transitions_msg, terminal_rewards_msg = transitions_queue.recv()
            transitions += transitions_msg
            tlist += terminal_rewards_msg

        if len(transitions) > 0:
            plot_durations(tlist)
            s, a, r, s_tag, is_terminal = [np.array(list(itertools.chain(*(zip(*t))))) for t in zip(*transitions)]
            rbuffer.append(s, a, r, s_tag, is_terminal)

    for i in range(100000):
        transitions = []
        while not transitions_queue.is_empty():
            transitions_msg, terminal_rewards_msg = transitions_queue.recv()
            transitions += transitions_msg
            tlist += terminal_rewards_msg

        if len(transitions) > 0:
            plot_durations(tlist)
            s, a, r, s_tag, is_terminal = [np.array(list(itertools.chain(*(zip(*t))))) for t in zip(*transitions)]
            rbuffer.append(s, a, r, s_tag, is_terminal)

        ep.value = len(tlist)
        for j in range(40):
            agent.net.zero_grad()

            state, action, reward, next_state, done = rbuffer.sample(min(len(rbuffer), MINIBATCH_SIZE))
            done = torch.from_numpy(done)
            next_state = torch.from_numpy(next_state)
            state = torch.from_numpy(state)
            action = torch.from_numpy(action)
            reward = torch.from_numpy(reward)

            loss = agent.loss(state, action, reward, next_state, done)
            loss.backward()

            global_optimizer.step()

            print('loss: %s; ep#: %s; updates#: %s; eps: %s' % (loss.item(), len(tlist), i, exploration.eps))

            if (j+1) % 20 == 0:
                agent.update_target()
