from __future__ import with_statement

import threading
from typing import Union, Iterable, Any

import numpy as np
from numpy import long

from rl_framework.src.common.state import State


class SimpleBuffer:
    def __init__(self):
        self._items = []
        
    def put(self, item):
        self._items.append(item)

    def __iadd__(self, items):
        self._items += items

    def get_all(self):
        items, self._items = self._items, []
        return items

    def __len__(self):
        return len(self._items)


class SynchronousBuffer(SimpleBuffer):
    def __init__(self):
        super().__init__()
        self._lock = threading.Lock()

    def put(self, item):
        with self._lock:
            super().put(item)

    def __iadd__(self, items):
        with self._lock:
            super().__iadd__(items)

    def get_all(self):
        with self._lock:
            return super().get_all()

    def __len__(self):
        with self._lock:
            super().__len__()


class NumpyRingBuffer:
    def __init__(self, max_len: int, empty_item: object):
        self._max_len = max_len
        self._idx = long(0)

        if isinstance(empty_item, np.ndarray):
            self._items = np.empty(shape=[max_len] + list(empty_item.shape), dtype=empty_item.dtype)
        else:
            self._items = np.empty(shape=max_len, dtype=type(empty_item))

    def put(self, item):
        self._items[self._idx % self._max_len] = item
        self._idx += 1

    def __iadd__(self, items):
        n = len(items)
        rel_idx = self._idx % self._max_len
        slots_to_end = self._max_len - rel_idx
        if n > self._max_len:
            self.__iadd__(items[-self._max_len:])
        elif n > slots_to_end:
            self.__iadd__(items[:slots_to_end])
            self.__iadd__(items[slots_to_end:])
        else:
            self._items[rel_idx:(rel_idx+n)] = items
            self._idx += n
        return self

    def get(self, idxs: Union[int, Iterable]):
        return self._items[idxs]

    def get_all(self):
        return self._items[:len(self)]

    def __str__(self):
        return str(self._items)

    def __len__(self):
        return min(self._max_len, self._idx)


class TransitionsRingBuffer:
    def __init__(self, max_len: int, state_object: Any):
        self.max_len = max_len
        self.s_buffer = NumpyRingBuffer(max_len, state_object)
        self.a_buffer = NumpyRingBuffer(max_len, np.empty(1).astype(np.long))
        self.r_buffer = NumpyRingBuffer(max_len, np.empty(1).astype(np.float32))
        self.s_tag_buffer = NumpyRingBuffer(max_len, state_object)
        self.is_terminal_buffer = NumpyRingBuffer(max_len, np.empty(1).astype(np.uint8))

    def put(self, s: State, a: int, r: float, s_tag: State, is_terminal: bool):
        self.s_buffer.put(s)
        self.a_buffer.put(a)
        self.r_buffer.put(r)
        self.s_tag_buffer.put(s_tag)
        self.is_terminal_buffer.put(is_terminal)

    def __iadd__(self, s: Iterable[State], a: Iterable[int], r: Iterable[float], s_tag: Iterable[State], is_terminal: Iterable[bool]):
        self.s_buffer += s
        self.a_buffer += a
        self.r_buffer += r
        self.s_tag_buffer += s_tag
        self.is_terminal_buffer += is_terminal

    def append(self, s: Iterable[State], a: Iterable[int], r: Iterable[float], s_tag: Iterable[State], is_terminal: Iterable[bool]):
        self.__iadd__(s, a, r, s_tag, is_terminal)

    def sample(self, size: int):
        if size > self.s_buffer._idx:
            return self.s_buffer.get_all(), self.a_buffer.get_all(), self.r_buffer.get_all(), \
                   self.s_tag_buffer.get_all(), self.is_terminal_buffer.get_all()
        else:
            idxs = np.random.randint(0, len(self.s_buffer), size)
            return self.s_buffer.get(idxs), self.a_buffer.get(idxs), self.r_buffer.get(idxs), \
                   self.s_tag_buffer.get(idxs), self.is_terminal_buffer.get(idxs)

    def __len__(self):
        return min(self.max_len, self.s_buffer._idx + 1)


class TransitionsSimpleBuffer:
    def __init__(self):
        self.s_buffer = SimpleBuffer()
        self.a_buffer = SimpleBuffer()
        self.r_buffer = SimpleBuffer()
        self.s_tag_buffer = SimpleBuffer()
        self.is_terminal_buffer = SimpleBuffer()

    def put(self, s: State, a: int, r: float, s_tag: State, is_terminal: bool):
        self.s_buffer.put(s)
        self.a_buffer.put(a)
        self.r_buffer.put(r)
        self.s_tag_buffer.put(s_tag)
        self.is_terminal_buffer.put(is_terminal)

    def __iadd__(self, s: Iterable[State], a: Iterable[int], r: Iterable[float], s_tag: Iterable[State], is_terminal: Iterable[bool]):
        self.s_buffer += s
        self.a_buffer += a
        self.r_buffer += r
        self.s_tag_buffer += s_tag
        self.is_terminal_buffer += is_terminal

    def get_all(self):
        return self.s_buffer.get_all(), self.a_buffer.get_all(), self.r_buffer.get_all(), \
               self.s_tag_buffer.get_all(), self.is_terminal_buffer.get_all()

    def __len__(self):
        return len(self.s_buffer)
