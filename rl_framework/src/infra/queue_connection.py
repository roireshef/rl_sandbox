from collections import deque
from typing import Any, Optional

from rl_framework.src.infra.connection import Connection


class QueueConnection(Connection):
    def __init__(self, max_len: Optional[int] = None):
        self.q = deque(maxlen=max_len)

    def send(self, obj: Any) -> None:
        self.q.append(obj)

    def recv(self) -> Any:
        return self.q.popleft()

    def is_empty(self) -> bool:
        return not bool(self.q)
