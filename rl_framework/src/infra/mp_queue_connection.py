from typing import Any

from rl_framework.src.infra.connection import Connection
import torch.multiprocessing as mp


class MultiProcessQueueConnection(Connection):
    def __init__(self):
        self.q = mp.Queue()

    def send(self, obj: Any) -> None:
        self.q.put(obj)

    def recv(self) -> Any:
        return self.q.get()

    def is_empty(self) -> bool:
        return self.q.empty()
