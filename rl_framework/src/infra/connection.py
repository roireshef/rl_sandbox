from abc import ABC, abstractmethod
from typing import Any


class Connection(ABC):
    @abstractmethod
    def send(self, obj: Any) -> None:
        pass

    @abstractmethod
    def recv(self) -> Any:
        pass

    @abstractmethod
    def is_empty(self) -> bool:
        pass
