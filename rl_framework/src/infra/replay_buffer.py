import random
from collections import deque


class ReplayBuffer:
    def __init__(self, max_size):
        self.q = deque(maxlen=max_size)

    def append(self, o):
        self.q.append(o)

    def __iadd__(self, other):
        self.q += other
        return self

    def sample(self, batch_size):
        return random.sample(self.q, min(len(self.q), batch_size))

    def __len__(self):
        return len(self.q)
