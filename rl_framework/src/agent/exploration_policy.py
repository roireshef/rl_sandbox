from abc import abstractmethod, ABC
from multiprocessing.sharedctypes import Synchronized
from typing import List

from rl_framework.src.common.action import Action
import numpy as np


class ExplorationPolicy:
    @abstractmethod
    def update(self, num_episodes: int):
        pass

    @abstractmethod
    def choose_action(self, actions: List[Action], action_mask: List[bool], action_values: np.ndarray):
        pass


class EpsilonGreedy(ExplorationPolicy, ABC):
    def __init__(self, init_eps: float, min_eps: float, decay: float, shared_num_ep: Synchronized):
        self.init_eps = init_eps
        self.min_eps = min_eps
        self.decay = decay
        self._shared_num_ep = shared_num_ep

    @property
    def num_ep(self):
        return self._shared_num_ep.value

    @property
    def eps(self):
        return max(self.init_eps * self.decay ** self.num_ep, self.min_eps)

    def choose_action(self, actions: List[Action], action_mask: List[bool], action_values: np.ndarray,
                      only_exploit: bool=False):
        valid_actions_idxs = np.where(action_mask)[0]
        if np.random.uniform() < self.eps:
            # choose random action (explore)
            return self._explore_action(actions, valid_actions_idxs)
        else:
            # choose best action (exploit)
            return self._exploit_action(actions, valid_actions_idxs, action_values)

    def _explore_action(self, actions: List[Action], valid_actions_idxs: np.ndarray):
        return actions[np.random.choice(valid_actions_idxs)]

    @abstractmethod
    def _exploit_action(self, actions: List[Action], valid_actions_idxs: np.ndarray, action_values: np.ndarray):
        pass


class MaxEpsilonGreedy(EpsilonGreedy):
    def _exploit_action(self, actions: List[Action], valid_actions_idxs: np.ndarray, action_values: np.ndarray):
        max_ind = valid_actions_idxs[np.argmax(action_values[valid_actions_idxs])]
        return actions[max_ind]


class MinEpsilonGreedy(EpsilonGreedy):
    def _exploit_action(self, actions: List[Action], valid_actions_idxs: np.ndarray, action_values: np.ndarray):
        min_ind = valid_actions_idxs[np.argmax(action_values[valid_actions_idxs])]
        return actions[min_ind]
