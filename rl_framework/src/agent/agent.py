from abc import ABC, abstractmethod
from typing import List

from rl_framework.src.common.action import Action
from rl_framework.src.common.state import State


class Agent(ABC):
    @abstractmethod
    def choose_action_stochastic(self, state: State, actions: List[Action], action_mask: List[bool]) -> Action:
        pass

    @abstractmethod
    def choose_action_deterministic(self, state: State, actions: List[Action], action_mask: List[bool]) -> Action:
        pass

    @abstractmethod
    def update_num_episodes(self, num_episodes: int):
        pass
