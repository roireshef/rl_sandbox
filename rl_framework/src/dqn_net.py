from torch import nn


class DQNNet(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, num_actions: int):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.Tanh(),
            nn.Linear(hidden_size, hidden_size // 2),
            nn.Tanh(),
            nn.Linear(hidden_size // 2, num_actions)
        )
        DQNNet._set_init(self.layers.parameters())

    def forward(self, x):
        return self.layers(x)

    def loss(self, x):
        pass

    @staticmethod
    def _set_init(layers, std=1.0, bias=0.0):
        for layer in layers:
            if type(layer) == nn.Linear:
                nn.init.normal_(layer.weight, mean=0., std=std)
                nn.init.constant_(layer.bias, bias)
