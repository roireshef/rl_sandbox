from typing import Any
from typing import List

import gym
import numpy as np
import torch
from gym import spaces
from gym.envs.atari import AtariEnv

from rl_framework.src.common.action import Action
from rl_framework.src.common.environment import Environment
from rl_framework.src.common.state import State


class GymState(State):
    def __init__(self, raw_state: Any, is_terminal: bool):
        super().__init__(is_terminal)
        self.raw_state = raw_state

    def numpy(self):
        return np.array(self.raw_state, dtype=np.float32)

    def torch(self):
        return torch.Tensor(self.raw_state)


class GymAction(Action):
    def numpy(self):
        return np.array([self.id], dtype=np.long)

    def torch(self):
        return torch.Tensor([self.id])


class GymEnvironment(Environment):
    def __init__(self, environment_name: str):
        self.raw_env = gym.make(environment_name)
        self._current_state = self._reset()
        super().__init__()

    @property
    def is_atari(self):
        return isinstance(self.raw_env, gym.envs.atari.AtariEnv)

    @property
    def state(self):
        return self._current_state

    def get_valid_actions(self, state: GymState) -> (List[GymAction], List[bool]):
        return self.get_all_actions(), [True] * self.raw_env.action_space.n

    def get_all_actions(self) -> List[GymAction]:
        if not isinstance(self.raw_env.action_space, gym.spaces.Discrete):
            raise AttributeError('Can not enumerate continuous action space')

        return [GymAction(i) for i in range(self.raw_env.action_space.n)]

    def _step(self, action: GymAction) -> (GymState, float):
        state, reward, is_terminal, _ = self.raw_env.step(action.id)
        self._current_state = GymState(raw_state=state, is_terminal=is_terminal)
        return self._current_state, reward

    def _reset(self) -> GymState:
        self._current_state = GymState(raw_state=self.raw_env.reset(), is_terminal=False)
        return self._current_state


if __name__ == "__main__":
    env = GymEnvironment('CartPole-v0')
    env.reset()
    actions = env.get_all_actions()
    state = env.state

    env.raw_env.render()

