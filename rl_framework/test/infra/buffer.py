from rl_framework.src.infra.buffer import NumpyRingBuffer
import numpy as np

class foo:
    pass


class boo(foo):
    pass


def test_ring_complex_type():
    b = NumpyRingBuffer(10, foo())
    l = [foo()] * 7
    l2 = [boo()] * 8

    b += l
    b += l2


def test_ring_complex_type():
    state = np.random.random(size=5)
    b = NumpyRingBuffer(10, state)
    b.put(state)
    states2 = np.random.random(size=(6,5))
    states3 = np.random.random(size=(6,5))
    b += states2
    b += states3
