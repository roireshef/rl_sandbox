import math
import random
import time

import gym
import gym.spaces
import matplotlib.pyplot as plt
import ray
import torch
from gym.envs.atari import AtariEnv
from torch import nn
import numpy as np

from rl_framework.src.infra import ReplayBuffer

GAMMA = 0.99
GAME = "CartPole-v0"


class DQNModel(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, num_actions: int):
        super().__init__()
        self.s_dim = input_size
        self.a_dim = num_actions

        self._loss = torch.nn.MSELoss()

        self._layers = nn.Sequential(nn.Linear(input_size, hidden_size),
                                    nn.Tanh(),
                                    nn.Linear(hidden_size, hidden_size),
                                    nn.Tanh(),
                                    nn.Linear(hidden_size, num_actions))

        self._set_init(list(self._layers.children()))

    def forward(self, x):
        return self._layers(x)

    @torch.no_grad()
    def choose_action(self, s, eps=0):
        if np.random.random() <= eps:
            return random.sample(range(self.a_dim), 1)[0]
        else:
            return torch.argmax(self.forward(s)).item()

    def loss(self, s, a, r, s_tag, is_terminal):
        with torch.no_grad():
            q_max_tag, _ = torch.max(self.forward(s_tag), dim=-1)

        target = r + (~is_terminal).type(torch.float32) * GAMMA * q_max_tag.unsqueeze(-1)

        return self._loss(self.forward(s).gather(1, a).squeeze(1), target.squeeze(1)).sum()

    def _set_init(self, layers, std=0.1, bias=0.0):
        for layer in layers:
            if type(layer) == nn.Linear:
                nn.init.normal_(layer.weight, mean=0., std=std)
                nn.init.constant_(layer.bias, bias)


class Worker:
    def __init__(self, id: int, nun_hiddens: int):
        self.env = gym.make(GAME).unwrapped
        self.env.reset()

        self.model = DQNModel(self.env.observation_space.shape[0], nun_hiddens, self.env.action_space.n)

        self.id = id

        self.state_id = 0
        self.episode_id = 0

        self.cumulative_reward = 0

        self.epsilon_min = 0.01
        self.epsilon = 1.0
        self.epsilon_decay = 0.99

    @property
    def state(self):
        if type(self.env) == AtariEnv:
            return self.env._get_obs()
        else:
            return self.env.state

    def get_epsilon(self, t):
        return max(self.epsilon_min, min(self.epsilon, 1.0 - math.log10((t + 1) * self.epsilon_decay)))

    def step(self):
        s = torch.FloatTensor(self.state)
        a = self.model.choose_action(s.unsqueeze(0), self.get_epsilon(self.episode_id))

        s_tag, r, is_terminal, info = self.env.step(a)
        self.state_id += 1
        self.cumulative_reward += r

        state_id = self.state_id

        info = {'worker_id': self.id, 'state_id': state_id, 'cumulative_reward': self.cumulative_reward}

        if self.state_id >= 200:
            is_terminal = True

        if is_terminal:
            self.env.reset()
            self.episode_id += 1
            self.state_id = 0
            self.cumulative_reward = 0

        return s.unsqueeze(0), torch.LongTensor([a]), torch.FloatTensor([r]), torch.Tensor([s_tag]), is_terminal, info

    def play(self, parameters=None):
        if parameters is not None:
            self.update(parameters)

        is_terminal = False
        buf = []
        while not is_terminal:
            transaction = self.step()
            is_terminal = transaction[-2]
            buf.append(transaction)

        return buf

    def update(self, parameters=None):
        self.model.load_state_dict(parameters)


@ray.remote
class RemoteWorker(Worker):
    pass


def plot_progress(episode_durations):
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Score')
    plt.plot(durations_t.numpy())
    # Take 100 episode averages and plot them too
    if len(durations_t) >= 20:
        means = durations_t.unfold(0, 20, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(19), means))
        plt.plot(means.numpy())

    plt.pause(0.00001)  # pause a bit so that plots are updated


def render_global_env(env, global_model):
    env.reset()
    is_terminal = False
    t = 0
    while not is_terminal and t < 100:
        s = torch.FloatTensor(env._get_obs() if type(env) == AtariEnv else env.state)
        a = global_model.choose_action(s.unsqueeze(0))

        _, _, is_terminal, _ = env.step(a)
        env.render()
        time.sleep(3e-4)
        t += 1


if __name__ == "__main__":
    NUM_HIDDENS = 40
    BATCH_SIZE = 1024
    WORKERS = 14
    ALPHA = 0.01
    ALPHA_DECAY = 0.01

    ray.init()

    local_worker = Worker(-1, NUM_HIDDENS)
    local_optimizer = torch.optim.Adam(local_worker.model.parameters(), lr=ALPHA, weight_decay=ALPHA_DECAY)

    # initialize remote workers
    rollout_workers = [RemoteWorker.remote(i, NUM_HIDDENS) for i in range(WORKERS)]

    # perform first steps
    future_ids = [rw.play.remote(local_worker.model.state_dict()) for rw in rollout_workers]

    # for debug & monitoring
    tlist = []
    update_workers = []
    replay_buffer = ReplayBuffer(100000)
    for t in range(1000000):
        done_ids, future_ids = ray.wait(future_ids)
        done_results = ray.get(done_ids)

        flat_results = [item for sublist in done_results for item in sublist]
        replay_buffer += flat_results

        # report episode durations
        worker_ids = set()
        for i in range(len(flat_results)):
            info = flat_results[i][-1]
            is_terminal = flat_results[i][-2]
            worker_id = info['worker_id']
            worker_ids.add(worker_id)
            if is_terminal:
                tlist.append(info['cumulative_reward'])
                plot_progress(tlist)

        s, a, r, s_tag, is_terminal, info = [list(tup) for tup in zip(*replay_buffer.sample(BATCH_SIZE))]

        local_worker.model.zero_grad()
        loss = local_worker.model.loss(torch.cat(s, dim=0),
                                       torch.cat(a, dim=0).unsqueeze(-1),
                                       torch.cat(r, dim=0).unsqueeze(-1),
                                       torch.cat(s_tag, dim=0),
                                       torch.ByteTensor(is_terminal).unsqueeze(-1))

        loss.backward()
        local_optimizer.step()

        for wid in worker_ids:
            future_ids.append(rollout_workers[wid].play.remote(local_worker.model.state_dict()))

        if t % 1000 == 0:
            render_global_env(local_worker.env, local_worker.model)

    [ray.shutdown(w) for w in rollout_workers]

