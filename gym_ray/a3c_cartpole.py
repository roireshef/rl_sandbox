import abc
import copy
from itertools import count

import gym
import gym.spaces
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn

import random


class Env:
    @property
    @abc.abstractmethod
    def num_actions(self) -> int:
        pass

    @property
    @abc.abstractmethod
    def state_space_dims(self) -> int:
        pass

    def current_state(self):
        pass

    def step(self, action) -> (np.array, float, bool):
        pass


class CyclicCartPoleEnv(Env):
    def __init__(self):
        # os.environ["MKL_NUM_THREADS"] = "1"
        self.gym_env = gym.make("CartPole-v1")
        self.gym_env.reset()

    @property
    def num_actions(self) -> int:
        return self.gym_env.action_space.n

    @property
    def state_space_dims(self) -> int:
        return self.gym_env.observation_space.shape[0]

    @property
    def current_state(self):
        return self.gym_env.env.state

    def step(self, action) -> (np.array, float, bool):
        state, reward, is_terminal, info = self.gym_env.step(action)
        if is_terminal:
            self.gym_env.reset()

        return state, reward, is_terminal


# class A3CModel(nn.Module):
#     def __init__(self, input_size: int, hidden_size: int, num_actions: int):
#         super().__init__()
#         self.fc_policy1 = nn.Linear(input_size, hidden_size)
#         self.fc_policy2 = nn.Linear(hidden_size, num_actions)
#         self.fc_value1 = nn.Linear(input_size, hidden_size)
#         self.fc_value2 = nn.Linear(hidden_size, 1)
#         self._set_init([self.fc_policy1, self.fc_policy2, self.fc_value1, self.fc_value2])
#
#     def forward(self, state):
#         policy_head = F.softmax(self.fc_policy2(F.relu(self.fc_policy1(state))))
#         value_head = self.fc_value2(F.relu(self.fc_value1(state)))
#         return policy_head, value_head
#
#     def loss(self, state, action, reward, next_state):
#         policy_output, state_value = self(state)
#         with torch.no_grad():
#             next_state_value = self(next_state)[1] if next_state is not None else torch.FloatTensor([0])
#
#         advantage = (reward + next_state_value - state_value)
#
#         v_loss = advantage.pow(2)
#         a_loss = -1 * torch.log(policy_output[action]) * advantage.detach().squeeze()
#
#         return (a_loss + v_loss).mean()
#
#     def _set_init(self, layers):
#         for layer in layers:
#             nn.init.normal_(layer.weight, mean=0., std=0.1)
#             nn.init.constant_(layer.bias, 0.1)

class A3CModel(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, num_actions: int):
        super().__init__()
        self.s_dim = input_size
        self.a_dim = num_actions
        self.pi1 = nn.Linear(input_size, hidden_size)
        self.pi2 = nn.Linear(hidden_size, num_actions)
        self.v1 = nn.Linear(input_size, hidden_size)
        self.v2 = nn.Linear(hidden_size, 1)
        self._set_init([self.pi1, self.pi2, self.v1, self.v2])
        self.distribution = torch.distributions.Categorical

    def forward(self, x):
        pi1 = F.relu(self.pi1(x))
        logits = self.pi2(pi1)
        v1 = F.relu(self.v1(x))
        values = self.v2(v1)
        return logits, values

    def choose_action(self, s):
        self.eval()
        logits, _ = self.forward(s)
        prob = F.softmax(logits, dim=1).data
        try:
            m = self.distribution(prob)
            return m.sample().numpy()[0]
        except Exception:
            m = self.distribution(torch.div(torch.ones_like(prob), torch.FloatTensor([self.a_dim])))
            print('here!!')
            return m.sample().numpy()[0]

    def loss(self, s, a, r, s_tag):
        self.eval()
        v_tag = self.forward(s_tag)[1] if s_tag is not None else torch.tensor([0.0])

        self.train()
        logits, values = self.forward(s)

        td = (v_tag + r).detach() - values
        c_loss = td.pow(2)

        probs = F.softmax(logits, dim=1)
        m = self.distribution(probs)
        exp_v = m.log_prob(a) * td.detach().squeeze()
        a_loss = -exp_v
        total_loss = (c_loss + a_loss).mean()

        return total_loss

    def _set_init(self, layers):
        for layer in layers:
            nn.init.normal_(layer.weight, mean=0., std=0.1)
            nn.init.constant_(layer.bias, 0.1)


class Agent:
    def __init__(self, model: nn.Module, optimizer: torch.optim.Optimizer):
        self.model = model
        self.optimizer = optimizer

    def get_action(self, state):
        """returns the index of the chosen action"""
        # with torch.no_grad():
        #     return torch.argmax(self.model(state)[0])
        return self.model.choose_action(state.unsqueeze(0))

    def compute_gradients(self, state: torch.Tensor, action: torch.Tensor, reward: torch.Tensor, next_state: torch.Tensor):
        self.optimizer.zero_grad()
        self.model.loss(state,action,reward,next_state).backward()
        return {name: w.grad.data for name, w in self.model.named_parameters()}

    def update(self, state_dict):
        self.model.load_state_dict(state_dict)


class Worker:
    def __init__(self, agent: Agent, env: Env):
        self.env = env
        self.agent = agent

    def step(self):
        s = torch.FloatTensor(self.env.current_state)
        a = self.agent.get_action(s)

        s_tag, r, is_terminal = self.env.step(a)

        return self.agent.compute_gradients(s.unsqueeze(0), torch.IntTensor([a]), torch.FloatTensor([r]),
                                            torch.Tensor([s_tag]) if not is_terminal else None), is_terminal


def plot_durations(episode_durations):
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    plt.plot(durations_t.numpy())
    # Take 100 episode averages and plot them too
    if len(durations_t) >= 20:
        means = durations_t.unfold(0, 20, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(19), means))
        plt.plot(means.numpy())

    plt.pause(0.00001)  # pause a bit so that plots are updated


if __name__ == "__main__":
    NUM_HIDDENS = 100

    global_cartpole_env = CyclicCartPoleEnv()
    global_model = A3CModel(input_size=global_cartpole_env.state_space_dims,
                            hidden_size=NUM_HIDDENS,
                            num_actions=global_cartpole_env.num_actions)
    global_optimizer = torch.optim.SGD(global_model.parameters(), lr=10e-3)
    global_agent = Agent(global_model, global_optimizer)
    global_worker = Worker(global_agent, global_cartpole_env)

    rollout_worker = Worker(copy.deepcopy(global_agent), copy.deepcopy(global_cartpole_env))
    rollout_worker.agent.update(global_model.state_dict())

    # for debug
    rollout_worker.env.gym_env.render()

    tlist=[]
    for ep in range(1000):
        for t in count():
            grads, is_terminal = rollout_worker.step()

            global_optimizer.zero_grad()

            for k, w in global_model.named_parameters():
                w.grad = w.grad + grads[k] if w.grad is not None else grads[k]

            global_optimizer.step()
            rollout_worker.agent.update(global_model.state_dict())
            rollout_worker.env.gym_env.render()

            if is_terminal:
                break

        print(t)
        tlist.append(t)
        plot_durations(tlist)

