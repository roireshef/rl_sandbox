import time

import gym
import gym.spaces
import matplotlib.pyplot as plt
import ray
import torch
from gym.envs.atari import AtariEnv
from torch import nn
from torch.optim.lr_scheduler import ExponentialLR

GAMMA = 0.99
GAME = "CartPole-v0"


class A3CModel(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, num_actions: int, w_ent: float, w_critic: float):
        super().__init__()
        self.s_dim = input_size
        self.a_dim = num_actions

        self.fc_shared = nn.Sequential(nn.Linear(input_size, hidden_size),
                                       nn.ReLU(),
                                       nn.Linear(hidden_size, hidden_size),
                                       nn.ReLU())
        self.policy_head = nn.Sequential(nn.Linear(hidden_size, num_actions),
                                         nn.Softmax(dim=1))
        self.value_head = nn.Sequential(nn.Linear(hidden_size, hidden_size),
                                        nn.ReLU(),
                                        nn.Linear(hidden_size, 1))

        self._set_init(list(self.fc_shared.children()), std=0.2, bias=0.1)
        self._set_init(list(self.policy_head.children()), std=0.2, bias=0.1)
        self._set_init(list(self.value_head.children()), std=0.1, bias=0.)
        self.distribution = torch.distributions.Categorical
        self.w_ent = w_ent
        self.w_critic = w_critic

    def forward(self, x):
        shared = self.fc_shared(x)
        probs = self.policy_head(shared)
        values = self.value_head(shared)

        return probs, values

    def choose_action_stochastic(self, s):
        with torch.no_grad():
            probs, _ = self.forward(s)
        act_dist = self.distribution(probs.data)  # P(a|s) - actor distribution
        return act_dist.sample().numpy()[0]

    def choose_action_deterministic(self, s):
        with torch.no_grad():
            probs, _ = self.forward(s)
        return torch.argmax(probs.data, dim=1)[0].numpy()

    def loss(self, s, a, r, s_tag, is_terminal):
        with torch.no_grad():
            v_s_tag = torch.zeros(len(s_tag), 1)
            v_s_tag[~is_terminal.squeeze()] = self.forward(s_tag[~is_terminal.squeeze()])[1]

        with torch.enable_grad():
            probs, v_s = self.forward(s)

        td = r.detach() + v_s_tag.detach() * GAMMA - v_s
        critic_loss = td.pow(2)

        actor_loss = -probs.gather(1, a).log() * td.detach()

        exploration_loss = (probs.log() * probs).sum(1)  # -1*entropy

        return critic_loss + actor_loss + self.w_ent * exploration_loss

    def _set_init(self, layers, std=0.1, bias=0.1):
        for layer in layers:
            if type(layer) == nn.Linear:
                nn.init.normal_(layer.weight, mean=0., std=std)
                nn.init.constant_(layer.bias, bias)


class Worker:
    def __init__(self, id: int, nun_hiddens: int, w_ent: float, w_critic: float):
        self.env = gym.make(GAME).unwrapped
        self.env.reset()

        self.model = A3CModel(self.env.observation_space.shape[0], nun_hiddens, self.env.action_space.n, w_ent, w_critic)

        self.id = id

        self.state_id = 0
        self.episode_id = 0

    @property
    def state(self):
        if type(self.env) == AtariEnv:
            return self.env._get_obs()
        else:
            return self.env.state

    def step(self):
        s = torch.FloatTensor(self.state)
        a = self.model.choose_action_stochastic(s.unsqueeze(0))

        s_tag, r, is_terminal, info = self.env.step(a)
        self.state_id += 1

        state_id = self.state_id

        if is_terminal or self.state_id >= 200:
            self.env.reset()
            self.episode_id += 1
            self.state_id = 0

        info = {'worker_id': self.id, 'state_id': state_id}

        return s.unsqueeze(0), torch.LongTensor([a]), torch.FloatTensor([r]), torch.Tensor([s_tag]), is_terminal, info

    def update(self, parameters=None):
        self.model.load_state_dict(parameters)


@ray.remote
class RemoteWorker(Worker):
    pass


def plot_durations(episode_durations):
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    plt.plot(durations_t.numpy())
    # Take 100 episode averages and plot them too
    if len(durations_t) >= 20:
        means = durations_t.unfold(0, 20, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(19), means))
        plt.plot(means.numpy())

    plt.pause(0.00001)  # pause a bit so that plots are updated


def render_global_env(env, global_model):
    env.reset()
    is_terminal = False
    t = 0
    while not is_terminal and t < 1000:
        s = torch.FloatTensor(env._get_obs() if type(env) == AtariEnv else env.state)
        a = global_model.choose_action_deterministic(s.unsqueeze(0))

        _, _, is_terminal, _ = env.step(a)
        env.render()
        time.sleep(3e-4)
        t += 1


## THIS IS NOT A STABLE VERSION !! ##
## MAYBE BECAUSE GRADIENTS ARE COMPUTED LOCALLY ##


if __name__ == "__main__":
    NUM_HIDDENS = 100
    MIN_BATCH = 8
    WORKERS = 32
    ENT_COEF = 3
    CRITIC_COEF = 2

    ray.init()

    env = gym.make(GAME).unwrapped
    env.reset()
    global_worker = Worker(-1, NUM_HIDDENS, ENT_COEF, CRITIC_COEF)
    global_optimizer = torch.optim.SGD(global_worker.model.parameters(), lr=1e-3)
    global_scheduler = ExponentialLR(global_optimizer, gamma=0.995)

    rollout_workers = [RemoteWorker.remote(i, NUM_HIDDENS, ENT_COEF, CRITIC_COEF) for i in range(WORKERS)]

    [rw.update.remote(global_worker.model.state_dict()) for rw in rollout_workers]

    future_ids = [rw.step.remote() for rw in rollout_workers]

    # for debug & monitoring
    tlist = []
    update_workers = []
    num_samples = 0
    s_buf = []
    a_buf = []
    r_buf = []
    s_tag_buf = []
    is_terminal_buf = []
    for t in range(1000000):
        done_ids, future_ids = ray.wait(future_ids, 1)
        done_results = ray.get(done_ids)

        s, a, r, s_tag, is_terminal, info = [list(tup) for tup in zip(*done_results)]

        s_buf += s
        a_buf += a
        r_buf += r
        s_tag_buf += s_tag
        is_terminal_buf += is_terminal

        # report episode durations
        worker_ids = []
        for i in range(len(done_results)):
            worker_id = info[i]['worker_id']
            worker_ids.append(worker_id)
            if is_terminal[i]:
                tlist.append(info[i]['state_id'])
                plot_durations(tlist)
                global_scheduler.step()

        num_samples += len(done_results)

        if num_samples >= MIN_BATCH:
            global_worker.model.zero_grad()
            loss = global_worker.model.loss(torch.cat(s_buf, dim=0),
                                            torch.cat(a_buf, dim=0).unsqueeze(-1),
                                            torch.cat(r_buf, dim=0).unsqueeze(-1),
                                            torch.cat(s_tag_buf, dim=0),
                                            torch.ByteTensor(is_terminal_buf).unsqueeze(-1)).mean().backward()

            s_buf.clear()
            a_buf.clear()
            r_buf.clear()
            s_tag_buf.clear()
            is_terminal_buf.clear()

            num_samples = 0

            global_optimizer.step()
            global_optimizer.zero_grad()

            updates = [w.update.remote(global_worker.model.state_dict()) for w in rollout_workers]
            # ray.wait(updates, len(rollout_workers))

        for wid in worker_ids:
            future_ids.append(rollout_workers[wid].step.remote())

        if t % 10000 == 0:
            render_global_env(env, global_worker.model)

    [ray.shutdown(w) for w in rollout_workers]

