import ray
import ray.tune.registry as RLLibRegistry
from flow_rl_framework.agent import a3c
from flow_rl_framework.agent.a3c import DEFAULT_CONFIG as A3C_DEFAULT_CONFIG
from ray.rllib.models import ModelCatalog
from ray.tune.logger import pretty_print

from flow_rl_framework.environments.zipper_merge_init import get_instance
from flow_rl_framework.models.merge_model import MergeModel

if __name__ == "__main__":
    ray.init()

    RLLibRegistry.register_env("SumoMerge-v0", lambda env_context: get_instance(worker_index=env_context.worker_index,
                                                                                render=False))
    ModelCatalog.register_custom_model("merge_model", MergeModel)

    config = A3C_DEFAULT_CONFIG
    config.update({
        "use_pytorch": True,
        "num_workers": 8,
        "gamma": 0.99,
        "entropy_coeff": 0.01,
        "min_iter_time_s": 2,
        "model": {
            "custom_model": "merge_model",
            "custom_options": {
                "linear_hidden_size": 200,
                "host_hidden_size": 20,
                "lstm_cell_size": 32
            }
        }
    })

    trainer = a3c.A3CAgent(env='SumoMerge-v0', config=config)
    trainer.restore('/home/roi/ray_results/A3C_SumoMerge-v0_2019-06-05_08-49-08_hlrwasd/checkpoint_51/checkpoint-51')

    for i in range(10000):
        result = trainer.train()
        print(pretty_print(result))

        if i > 0 and i % 50 == 0:
            checkpoint = trainer.save()
            print("checkpoint saved at", checkpoint)
