import gym
from flow.controllers import ContinuousRouter, RLController
from flow.controllers.car_following_models import IDMController
from flow.core.params import NetParams
from flow.core.params import SumoParams, EnvParams, \
    InFlows, SumoCarFollowingParams, VehicleParams

from flow_rl_framework.environments.zipper_merge import ADDITIONAL_ENV_PARAMS, ZipperMergeEnv
from flow_rl_framework.environments.zipper_merge import ZipperMergeScenario
import os


def params_and_scenario(render=False, restart=False):
    env_params = EnvParams(
        additional_params=ADDITIONAL_ENV_PARAMS,
        sims_per_step=5,
        warmup_steps=0)

    sim_params = SumoParams(
        render=render,
        # emission_path="./data/%d" % instance,
        restart_instance=restart)

    vehicles = VehicleParams()
    vehicles.add(
        veh_id="rl",
        acceleration_controller=(RLController, {}),
        car_following_params=SumoCarFollowingParams(speed_mode="aggressive", min_gap=0),
        routing_controller=(ContinuousRouter, {}),
        num_vehicles=1)

    vehicles.add(
        veh_id="human_slow",
        acceleration_controller=(IDMController, {"noise": 0.5, "v0": 15, "T": 1.0, "dt": 1.0}),
        car_following_params=SumoCarFollowingParams(speed_mode="aggressive", min_gap=0, car_follow_model='Krauss'),
        routing_controller=(ContinuousRouter, {}),
        num_vehicles=0)

    vehicles.add(
        veh_id="human_fast",
        acceleration_controller=(IDMController, {"noise": 0.5, "v0": 15, "T": 0.3, "dt": 1.0}),
        car_following_params=SumoCarFollowingParams(speed_mode="aggressive", min_gap=0, car_follow_model='Krauss'),
        routing_controller=(ContinuousRouter, {}),
        num_vehicles=0)

    inflows = InFlows()
    inflows.add(
        veh_type="human_slow",
        edge="hwy_in",
        probability=0.2,
        departSpeed=15,
        color="1,1,0")
    inflows.add(
        veh_type="human_fast",
        edge="hwy_in",
        probability=0.2,
        departSpeed=15,
        color="1,1,0")

    net_params = NetParams(netfile=os.getcwd()+'/references/simple_zipper_merge2.net.xml')
    net_params.no_internal_links = False
    net_params.inflows = inflows

    scenario = ZipperMergeScenario(name='ZipperMergeScenario', vehicles=vehicles, net_params=net_params)

    return env_params, sim_params, scenario


def register_gym_env(render=False, restart=False):
    env_params, sim_params, scenario = params_and_scenario(render, restart)
    gym.envs.register(
        id='SumoMerge-v0',
        entry_point="flow_rl_framework.environments.zipper_merge:ZipperMergeEnv",
        kwargs={
            "env_params": env_params,
            "sim_params": sim_params,
            "scenario": scenario
        }
    )


def get_instance(worker_index=0, render=False, restart=False):
    env_params, sim_params, scenario = params_and_scenario(render, restart)
    sim_params.port = 10000 + worker_index
    return ZipperMergeEnv(env_params, sim_params, scenario)


