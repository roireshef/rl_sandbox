import numpy as np
from flow.core.params import InitialConfig
from flow.core.params import TrafficLightParams
from flow.envs import Env
from flow.scenarios import Scenario
from gym.spaces.box import Box
from gym.spaces.discrete import Discrete
from gym.spaces.tuple_space import Tuple

ADDITIONAL_ENV_PARAMS = {
    "max_actors_in_frame": 10,
    "actors_edge_id": 'hwy_in',
    "host_edge_id": 'on_ramp',
    "target_edge_id": 'hwy_out',
    "maximal_velocity": 30.0,
    "cut_in_allowed_headway": 1.0,
    "penalty_score": -1,
    "success_score": 1
}


class ZipperMergeScenario(Scenario):
    def __init__(self,
                 name,
                 vehicles,
                 net_params,
                 initial_config=InitialConfig(),
                 traffic_lights=TrafficLightParams()):
        initial_config.spacing = "custom"
        super().__init__(name, vehicles, net_params, initial_config, traffic_lights)

    @staticmethod
    def gen_custom_start_pos(cls, net_params, initial_config, num_vehicles):
        return [('on_ramp', 0)], [0]

    def specify_routes(self, net_params):
        rts = {
            "hwy_in": ["hwy_in", "hwy_out"],
            "hwy_out": ["hwy_out"],
            "on_ramp": ["on_ramp", "hwy_out"]
        }
        return rts


class ZipperMergeEnv(Env):
    def __init__(self, env_params, sim_params, scenario):
        super().__init__(env_params, sim_params, scenario)

        self.last_host_velocity = 0
        self.action_horizon = 3  # sec

    @property
    def host_id(self):
        return self.k.vehicle.get_rl_ids()[0]

    ##############################################
    ### Overrides - environment implementation ###
    ##############################################

    @property
    def action_space(self):
        """ 6 commands available: reaching [0%, 20%, ..., 100%] of max velocity
            acceleration will be calculated by attempting to reach the target velocity in a <self.action_horizon> secs
        """
        return Discrete(n=5)

    @property
    def observation_space(self):
        """
        state space includes:
        First Box   - host vehicle's (distance to junction, velocity, acceleration)
        Second Box  - counter for actors (used to crop Third Box)
        Third Box   - all actors on the target lane, ordered by proximity to the junction, each one has:
                    (distance to junction,
                    velocity - longitudinal,
                    acceleration - longitudinal)
        Fourth Box - 1d array with:
                    - maximal distance to junction (actors lane)
                    - maximal velocity
        """
        return Tuple(spaces=(
            Box(low=0, high=np.inf, shape=(3,), dtype=np.float32),
            Box(low=0, high=self.env_params.additional_params["max_actors_in_frame"], shape=(1,), dtype=np.int),
            Box(low=0, high=np.inf, shape=(self.env_params.additional_params["max_actors_in_frame"], 2),
                dtype=np.float32),
            Box(low=0, high=np.inf, shape=(2,), dtype=np.float32)))

    def get_state(self):
        maximal_actors_distance = self.k.scenario.edge_length(self.env_params.additional_params["actors_edge_id"])
        maximal_host_distance = self.k.scenario.edge_length(self.env_params.additional_params["host_edge_id"])

        host_state = [self._get_dist_to_junction(self.host_id) / maximal_host_distance,
                      self.k.vehicle.get_speed(self.host_id),
                      self._get_host_acceleration()]

        actors_state = np.zeros(self.observation_space.spaces[2].shape)
        # TODO: add acceleration (replace 0.0)
        actors_state_list = [
            [self._get_dist_to_junction(veh_id) / maximal_actors_distance,
             self.k.vehicle.get_speed(veh_id) / self.env_params.additional_params["maximal_velocity"]]
            for veh_id in self.k.vehicle.get_human_ids()
            if self._get_dist_to_junction(veh_id) >= 0
        ]
        if len(actors_state_list) > 0:
            actors_state_list.reverse()
            actors_state[:len(actors_state_list)] = actors_state_list

        map_state = [maximal_actors_distance, self.env_params.additional_params["maximal_velocity"]]

        return np.array(host_state), np.array([len(actors_state_list)]), np.array(actors_state), np.array(map_state)

    def compute_reward(self, rl_actions, **kwargs) -> (float, bool):
        done = kwargs.get('fail', False)
        reward = 0

        current_follower = self.k.vehicle.get_follower(self.host_id)
        current_leader = self.k.vehicle.get_leader(self.host_id)

        # sumo reports crash!
        if done:
            reward = self.env_params.additional_params["penalty_score"]

        # crash in merge or unsafe cut-in
        if current_leader is not None and current_leader == current_follower or \
                not self._is_host_safe_wrt(current_leader, current_follower):
            done = True
            reward = self.env_params.additional_params["penalty_score"]

        # RL agent is past the merge
        if self._get_dist_to_junction(self.host_id) < 0:
            done = True
            reward = self.env_params.additional_params["success_score"]

        return reward, done

    def _apply_rl_actions(self, rl_actions):
        current_velocity = self.k.vehicle.get_speed(self.host_id)

        self.last_host_velocity = current_velocity

        target_velocity = self.env_params.additional_params["maximal_velocity"] * float(
            rl_actions) / self.action_space.n
        acceleration = (target_velocity - current_velocity) / self.action_horizon

        self.k.vehicle.apply_acceleration([self.host_id], [acceleration])

    #################
    ### Utilities ###
    #################

    def _is_host_safe_wrt(self, leader_id, follower_id):
        return (follower_id is None or self.k.vehicle.get_headway(follower_id) > self.env_params.additional_params[
            "cut_in_allowed_headway"]) \
               and (leader_id is None or self.k.vehicle.get_headway(self.host_id) > self.env_params.additional_params[
            "cut_in_allowed_headway"])

    def _get_host_acceleration(self):
        return (self.k.vehicle.get_speed(self.host_id) - self.last_host_velocity) / self.k.vehicle.get_timedelta(
            self.host_id)

    def _get_dist_to_junction(self, veh_id):
        edge_id = self.k.vehicle.get_edge(veh_id)
        if edge_id in ['hwy_in', 'on_ramp']:
            edge_len = self.k.scenario.edge_length(edge_id)
            relative_pos = self.k.vehicle.get_position(veh_id)
            return edge_len - relative_pos
        elif 'merge' in edge_id:
            return 0
        else:
            return -self.k.vehicle.get_position(veh_id)
