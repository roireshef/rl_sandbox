import gym
import numpy as np
from ray.rllib.models.model import _unpack_obs, _restore_original_dimensions
from ray.rllib.models.preprocessors import TupleFlatteningPreprocessor

from flow_rl_framework.environments.zipper_merge_init import register_gym_env
from flow_rl_framework.models.merge_model import MergeModel

import torch

if __name__ == "__main__":
    register_gym_env(True, False)

    num_nodes = 1

    envs = [gym.make('SumoMerge-v0') for i in range(num_nodes)]
    [env.reset() for env in envs]

    max_steps = 1000000

    vel = np.zeros(max_steps)
    ret = 0
    ret_list = []

    options = {"custom_options": {"lstm_cell_size": 64, "linear_hidden_size": 40, "host_hidden_size": 8}}
    net = MergeModel(envs[0].observation_space, envs[0].action_space.n, options)
    flattener = TupleFlatteningPreprocessor(net.obs_space)
    unpack = lambda x: _unpack_obs(x, net.obs_space, tensorlib=torch)

    for j in range(max_steps):
        for n in range(num_nodes):
            state, reward, done, info = envs[n].step(2)

            hidden_state = net.state_init()
            input_dict = {"obs": torch.tensor([flattener.transform(state)])}

            input_dict["obs"] = unpack(input_dict["obs"].float())
            outputs, features, vf, h = net._forward(input_dict, hidden_state)

            vel[j] = envs[n].k.vehicle.get_speed(envs[n].k.vehicle.get_rl_ids()[0])

            ret += reward
            ret_list.append(reward)

            if done:
                envs[n].reset()
                print('simulation terminated at step %d with reward %f' % (j, reward))