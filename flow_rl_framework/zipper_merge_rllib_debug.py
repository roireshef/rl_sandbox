import gym
from ray.rllib import PolicyEvaluator
from ray.rllib.agents.a3c.a3c_torch_policy_graph import A3CTorchPolicyGraph
from ray.rllib.evaluation.sampler import SyncSampler
from ray.tune.registry import register_env as register_env_rllib

from flow_rl_framework.environments.zipper_merge_init import get_instance, register_gym_env
from flow_rl_framework.models.merge_model import MergeModel

if __name__ == "__main__":
    register_env_rllib("SumoMerge-v0", lambda _: get_instance())
    register_gym_env()

    env = gym.make("SumoMerge-v0")
    options_dict = {"custom_options": {"linear_hidden_size": 64, "lstm_cell_size": 32}}
    model = MergeModel(env.observation_space, env.action_space.n, options_dict)

    evaluator = PolicyEvaluator(
        env_creator=lambda _: gym.make("SumoMerge-v0"),
        policy_graph=A3CTorchPolicyGraph)

    sample = evaluator.sample()
    input_dict = {"obs": (sample.data["obs"][:, :3], sample.data["obs"][:, 3:4], sample.data["obs"][:, 4:34].reshape(-1,10,3), sample.data["obs"][:, 34:36])}

    output = model._forward(input_dict, model.state_init())







