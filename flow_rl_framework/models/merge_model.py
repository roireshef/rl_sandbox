import torch
from ray.rllib.models.pytorch.model import TorchModel
from torch import nn


class MergeModel(TorchModel):
    def __init__(self, obs_space, num_outputs, options):
        super().__init__(obs_space, num_outputs, options)
        host_space, num_actors_space, actors_space, metadata_space = obs_space.original_space.spaces

        custom_options = options["custom_options"]

        self.lstm_cell_size = custom_options["lstm_cell_size"]

        self.vehicles_lstm = nn.LSTM(actors_space.shape[1], self.lstm_cell_size, batch_first=True)
        self.fc_host = nn.Sequential(
            nn.Linear(host_space.shape[0], custom_options["host_hidden_size"]),
            nn.Tanh()
        )
        self.fc_shared = nn.Sequential(
            nn.Linear(custom_options["lstm_cell_size"] + custom_options["host_hidden_size"],
                      custom_options["linear_hidden_size"]),
            nn.Tanh(),
            nn.Linear(custom_options["linear_hidden_size"], custom_options["linear_hidden_size"]),
            nn.Tanh()
        )
        self.fc_actor_head = nn.Linear(custom_options["linear_hidden_size"], num_outputs)
        self.fc_critic_head = nn.Linear(custom_options["linear_hidden_size"], 1)

        self.apply(MergeModel._init_weights)

    @staticmethod
    def _init_weights(m):
        if type(m) == nn.Linear:
            torch.nn.init.xavier_normal_(m.weight)
            m.bias.data.fill_(0)
        elif type(m) == nn.LSTM:
            torch.nn.init.xavier_normal_(m.weight_ih_l0)
            torch.nn.init.xavier_normal_(m.weight_hh_l0)
            m.bias_ih_l0.data.fill_(0)
            m.bias_hh_l0.data.fill_(0)

    def _forward(self, input_dict, hidden_state):
        host_state, num_actors, actors_state, map_state = input_dict["obs"]
        h=torch.zeros(actors_state.shape[0], self.lstm_cell_size)
        for j in range(actors_state.shape[0]):
            seq_len = int(num_actors[j].item())
            if seq_len > 0:
                h[j] = self.vehicles_lstm(actors_state[j][:seq_len][None])[0][0, -1]
        host_emb = self.fc_host(host_state)
        actors_emb = torch.tanh(h)

        x = self.fc_shared(torch.cat((host_emb, actors_emb), -1))
        a = self.fc_actor_head(x)
        v = self.fc_critic_head(x).squeeze(0)

        return a, h, v, hidden_state

