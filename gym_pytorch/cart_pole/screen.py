import numpy as np
import torch
import torchvision.transforms as T
from itertools import count
from PIL import Image


class Screen:
    def __init__(self, env):
        self.env = env
        self._resize_op = T.Compose([T.ToPILImage(), T.Resize(100, interpolation=Image.CUBIC),  T.ToTensor()])
        self._device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def get_screen(self):
        # transpose into torch order (ColorHeightWidth)
        screen = self.env.render(mode='rgb_array').transpose((2, 0, 1))
        screen_arr = torch.from_numpy(np.ascontiguousarray(screen, dtype=np.float32) / 255)
        return self._resize(screen_arr)

    def _resize(self, img_arr, device='cuda'):
        """Resize, and add a batch dimension (BCHW)"""
        return self._resize_op(img_arr).unsqueeze(0).to(self._device)
