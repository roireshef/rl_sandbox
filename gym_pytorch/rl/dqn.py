# Inspired by https://keon.io/deep-q-learning/

from collections import deque
from ctypes import c_int

import gym
import matplotlib.pyplot as plt
import numpy as np
import torch
from gym.envs import register
from torch import nn

from rl_framework.src.agent.exploration_policy import MaxEpsilonGreedy
from rl_framework.src.dqn_net import DQNNet
from rl_framework.src.infra.buffer import TransitionsRingBuffer

import torch.multiprocessing as mp

def plot_durations(episode_durations):
    plt.figure(2)
    plt.clf()
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration')
    plt.plot(durations_t.numpy())
    # Take 100 episode averages and plot them too
    if len(durations_t) >= 100:
        means = durations_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy())

    plt.pause(0.00001)  # pause a bit so that plots are updated


register(
    id='CartPole-v3',
    entry_point='gym.envs.classic_control:CartPoleEnv',
    max_episode_steps=10000,
    reward_threshold=9900.0,
)


class DQNCartPoleSolver:
    def __init__(self, n_episodes=10000, n_win_ticks=9900, gamma=0.99, epsilon=1.0, epsilon_min=0.1,
                 epsilon_log_decay=0.9925, alpha=0.005, alpha_decay=0.1, batch_size=512):
        self.env = gym.make('CartPole-v3')
        self.memory = TransitionsRingBuffer(max_len=100000, state_object=np.empty(self.env.observation_space.shape[0], dtype=np.float32))
        self.gamma = gamma
        self.epsilon_max = self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_log_decay
        self.alpha = alpha
        self.alpha_decay = alpha_decay
        self.n_episodes = n_episodes
        self.n_win_ticks = n_win_ticks
        self.batch_size = batch_size

        # Init model
        self.model = DQNNet(4, 200, 2)
        self.target_model = DQNNet(4, 200, 2)
        self.update_target()

        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=self.alpha)
        self.loss = torch.nn.MSELoss()

    def update_target(self):
        self.target_model.load_state_dict(self.model.state_dict())

    def remember(self, state, action, reward, next_state, done):
        self.memory.append(state, action, reward, next_state, done)

    def choose_action(self, state, epsilon):
        with torch.no_grad():
            return self.env.action_space.sample() if (np.random.random() <= epsilon) \
                else torch.argmax(self.model.forward(torch.from_numpy(state).type(torch.float32))).item()

    def get_epsilon(self):
        return max(self.epsilon_min, self.epsilon)

    def set_epsilon(self, t):
        if self.epsilon > self.epsilon_min:
            self.epsilon = self.epsilon_max * self.epsilon_decay ** t

    def replay(self, batch_size):
        self.model.zero_grad()

        state, action, reward, next_state, done = self.memory.sample(min(len(self.memory), batch_size))
        done = torch.from_numpy(done)
        next_state = torch.from_numpy(next_state)
        state = torch.from_numpy(state)
        action = torch.from_numpy(action)
        reward = torch.from_numpy(reward)

        with torch.no_grad():
            target = reward
            target[~done] += self.gamma * torch.max(self.target_model.forward(next_state[~done.squeeze()]), dim=-1)[0]

        loss = self.loss(self.model.forward(state).gather(1, action), target).sum()
        loss.backward()

        self.optimizer.step()

    def run(self):
        scores = deque(maxlen=100)

        tlist = []
        for t in range(self.n_episodes):
            state = self.env.reset()
            done = False
            i = 0
            while not done:
                action = self.choose_action(state, self.get_epsilon())
                next_state, reward, done, _ = self.env.step(action)
                self.remember([state], [action], [reward], [next_state], [done])
                state = next_state
                i += 1

            scores.append(i)
            tlist.append(i)
            mean_score = np.mean(scores)
            if mean_score >= self.n_win_ticks and t >= 100:
                print('Ran {} episodes. Solved after {} trials ✔'.format(t, t - 100))
                return t - 100
            if t % 10 == 0:
                print('[Episode {}] - Mean survival time over last {} episodes was {} ticks (eps: {}).'.format(t, len(scores), mean_score, self.get_epsilon()))
                plot_durations(tlist)

            if len(self.memory) >= 1024:
                self.set_epsilon(t)
                for i in range(30):
                    self.replay(self.batch_size)
                    if i % 10 == 0:
                        self.update_target()

        print('Did not solve after {} episodes 😞'.format(t))
        return t


if __name__ == '__main__':
    agent = DQNCartPoleSolver()
agent.run()